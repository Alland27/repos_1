package co.edu.uniajc.backconcesionario.repositorio;


import co.edu.uniajc.backconcesionario.modelo.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepositorio extends JpaRepository<Cliente, Long> {


}
