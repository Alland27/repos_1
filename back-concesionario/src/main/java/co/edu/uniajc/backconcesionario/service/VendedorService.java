package co.edu.uniajc.backconcesionario.service;

import co.edu.uniajc.backconcesionario.modelo.Vendedor;
import co.edu.uniajc.backconcesionario.repositorio.VendedorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VendedorService {
    @Autowired
    private VendedorRepositorio vendedorRepositorio;

    public Vendedor createVendedor (Vendedor vendedor){
        return vendedorRepositorio.save(vendedor);
    }

    public List<Vendedor> ListarVendedores (){
        return vendedorRepositorio.findAll();
    }

    public Optional<Vendedor> VendedorporID (Long id){
        return vendedorRepositorio.findById(id);
    }

    public void  DeleteVendedor (Vendedor vendedor){
        vendedorRepositorio.delete(vendedor);
    }
}