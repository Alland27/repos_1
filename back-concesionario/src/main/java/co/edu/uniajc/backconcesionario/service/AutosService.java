package co.edu.uniajc.backconcesionario.service;


import co.edu.uniajc.backconcesionario.modelo.Auto;

import co.edu.uniajc.backconcesionario.modelo.Cliente;
import co.edu.uniajc.backconcesionario.repositorio.AutosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutosService {

    @Autowired
    private AutosRepositorio autosRepositorio;

    public Auto createAuto(Auto auto){
        return  autosRepositorio.save(auto);
    }

    public List<Auto> ListarAutos (){
        return autosRepositorio.findAll();
    }

    public Optional<Auto> AutoporID (Long id){
        return autosRepositorio.findById(id);
    }

    public void  DeleteAuto (Auto auto){
        autosRepositorio.delete(auto);

    }


}
