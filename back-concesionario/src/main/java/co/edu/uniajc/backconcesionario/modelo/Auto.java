package co.edu.uniajc.backconcesionario.modelo;

import javax.persistence.*;


    @Entity
    @Table(name = "autos")
    public class Auto {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id_auto")
        private Long Id_Auto;

        @Column(name = "marca")
        private String Marca;

        @Column(name = "linea")
        private String Linea;

        @Column(name = "tipo_aut")
        private String Tipo_Auto;

        @Column(name = "nro_chasis")
        private String Nro_Chasis;



        public Auto(Long Id_Auto, String Marca, String Linea,  String Tipo_Auto ,String Nro_Chasis  ) {
            this.Id_Auto = Id_Auto;
            this.Marca = Marca;
            this.Linea = Linea;
            this.Tipo_Auto = Tipo_Auto;
            this.Nro_Chasis = Nro_Chasis;

        }


        public Auto() {

        }

        public Long getId_Auto() {
            return Id_Auto;
        }

        public void setId_Auto(Long id_Auto) {
            Id_Auto = id_Auto;
        }

        public String getMarca() {
            return Marca;
        }

        public void setMarca(String marca) {
            Marca = marca;
        }

        public String getLinea() {
            return Linea;
        }

        public void setLinea(String linea) {
            Linea = linea;
        }

        public String getTipo_Auto() {
            return Tipo_Auto;
        }

        public void setTipo_Auto(String tipo_Auto) {
            Tipo_Auto = tipo_Auto;
        }

        public String getNro_Chasis() {
            return Nro_Chasis;
        }

        public void setNro_Chasis(String nro_Chasis) {
            Nro_Chasis = nro_Chasis;
        }
    }
