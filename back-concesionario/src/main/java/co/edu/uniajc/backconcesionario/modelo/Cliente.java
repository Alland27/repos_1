package co.edu.uniajc.backconcesionario.modelo;

import javax.persistence.*;

@Entity
@Table(name = "clientes")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "numero_documento")
    private Long Numero_Documento;

    @Column(name = "tipo_documento")
    private String Tipo_Documento;

    @Column(name = "nombre_cliente")
    private String Nombre_Cliente;

    @Column(name = "celular")
    private Long Celular;

    @Column(name = "direccion_cliente")
    private String Direccion_Cliente;

    @Column(name = "ciudad")
    private String Ciudad;



    public Cliente(Long Numero_Documento, String Tipo_Documento, String Nombre_Cliente, Long Celular, String Direccion_Cliente ,String Ciudad  ) {
        this.Numero_Documento = Numero_Documento;
        this.Tipo_Documento = Tipo_Documento;
        this.Nombre_Cliente = Nombre_Cliente;
        this.Celular = Celular;
        this.Direccion_Cliente = Direccion_Cliente;
        this.Ciudad = Ciudad;
    }


    public Cliente(){

    }

    public Long getNumero_Documento() {
        return Numero_Documento;
    }

    public void setNumero_Documento(Long numero_Documento) {
        Numero_Documento = numero_Documento;
    }

    public String getTipo_Documento() {
        return Tipo_Documento;
    }

    public void setTipo_Documento(String tipo_Documento) {
        Tipo_Documento = tipo_Documento;
    }

    public String getNombre_Cliente() {
        return Nombre_Cliente;
    }

    public void setNombre_Cliente(String nombre_Cliente) {
        Nombre_Cliente = nombre_Cliente;
    }

    public Long getCelular() {
        return Celular;
    }

    public void setCelular(Long celular) {
        Celular = celular;
    }

    public String getDireccion_Cliente() {
        return Direccion_Cliente;
    }

    public void setDireccion_Cliente(String direccion_Cliente) {
        Direccion_Cliente = direccion_Cliente;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String ciudad) {
        Ciudad = ciudad;
    }
}
