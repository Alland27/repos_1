package co.edu.uniajc.backconcesionario.repositorio;

import co.edu.uniajc.backconcesionario.modelo.Auto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutosRepositorio  extends JpaRepository<Auto, Long> {
}
