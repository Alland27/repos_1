package co.edu.uniajc.backconcesionario.controller;


import co.edu.uniajc.backconcesionario.modelo.Auto;

import co.edu.uniajc.backconcesionario.service.AutosService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/autos")
@Api("This API has a CRUD for Autos")
public class AutosController {
  @Autowired
  public AutosService autosService;

  public AutosController(AutosService service){
  }


 public AutosController(){

 }

    @GetMapping
    public ResponseEntity<List<Auto>> ListartodosAutos() {

      return ResponseEntity.ok(autosService.ListarAutos());
    }

    @PostMapping
    private ResponseEntity<Auto> guardar(@RequestBody Auto auto) {
        Auto temp = autosService.createAuto(auto);
        try {
            return ResponseEntity.created(new URI("/api/auto/" + temp.getId_Auto())).body(temp);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping
    private ResponseEntity<Void> EliminarAutos(@RequestBody Auto auto) {
        autosService.DeleteAuto(auto);
        return ResponseEntity.ok().build();
    }




}
