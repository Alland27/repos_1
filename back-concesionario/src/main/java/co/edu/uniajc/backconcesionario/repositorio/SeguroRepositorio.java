package co.edu.uniajc.backconcesionario.repositorio;

import co.edu.uniajc.backconcesionario.modelo.Seguro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeguroRepositorio extends JpaRepository<Seguro, Long> {
}
