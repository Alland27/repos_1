package co.edu.uniajc.backconcesionario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackConcesionarioApplication {

	public static void main(String[] args) {

		SpringApplication.run(BackConcesionarioApplication.class, args);
	}

}
