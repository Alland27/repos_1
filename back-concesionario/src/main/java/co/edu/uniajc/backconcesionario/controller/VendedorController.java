package co.edu.uniajc.backconcesionario.controller;

import co.edu.uniajc.backconcesionario.modelo.Vendedor;
import co.edu.uniajc.backconcesionario.service.VendedorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vendedor")
public class VendedorController {
    @Autowired
    public VendedorService vendedorService;

    public VendedorController(VendedorService service) {}

    public VendedorController() {}

    @PostMapping
    private ResponseEntity<Vendedor> guardar(@RequestBody Vendedor vendedor) {
        Vendedor temp = vendedorService.createVendedor(vendedor);
        try {
            return ResponseEntity.created(new URI("api/vendedor/" + temp.getTipo_Documento())).body(temp);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Vendedor>> ListarTodosVendedores() {
        return ResponseEntity.ok(vendedorService.ListarVendedores());
    }

    @GetMapping(value = "{id}")
    private ResponseEntity<Optional<Vendedor>> ListarClientesPorID(@PathVariable("id") Long id) {
        return ResponseEntity.ok(vendedorService.VendedorporID(id));
    }

    @DeleteMapping
    private ResponseEntity<Void> EliminarCliente(@RequestBody Vendedor vendedor) {
        vendedorService.DeleteVendedor(vendedor);
        return ResponseEntity.ok().build();
    }

    public String analysemood(String message){
        return "sad";
    }
}
