package co.edu.uniajc.backconcesionario.modelo;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Seguro")
public class Seguro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_seguro")
    private Long Id_Seguro;

    @Column(name = "id_auto")
    private Long Id_Auto;

    @Column(name = "id_propietario")
    private Long Id_Propietario;

    @Column(name = "fecha_expedicion")
    private Date Fecha_Expedicion;

    @Column(name = "fecha_vencimiento")
    private Date Fecha_Vencimiento;

    public Seguro(Long id_Seguro, Long id_Auto, Long id_Propietario, Date fecha_Expedicion, Date fecha_Vencimiento) {
        Id_Seguro = id_Seguro;
        Id_Auto = id_Auto;
        Id_Propietario = id_Propietario;
        Fecha_Expedicion = fecha_Expedicion;
        Fecha_Vencimiento = fecha_Vencimiento;
    }

    public Seguro() {
    }

    public Long getId_Seguro() {
        return Id_Seguro;
    }

    public Long getId_Auto() {
        return Id_Auto;
    }

    public Long getId_Propietario() {
        return Id_Propietario;
    }

    public Date getFecha_Expedicion() {
        return Fecha_Expedicion;
    }

    public Date getFecha_Vencimiento() {
        return Fecha_Vencimiento;
    }

    public void setId_Seguro(Long id_Seguro) {
        Id_Seguro = id_Seguro;
    }

    public void setId_Auto(Long id_Auto) {
        Id_Auto = id_Auto;
    }

    public void setId_Propietario(Long id_Propietario) {
        Id_Propietario = id_Propietario;
    }

    public void setFecha_Expedicion(Date fecha_Expedicion) {
        Fecha_Expedicion = fecha_Expedicion;
    }

    public void setFecha_Vencimiento(Date fecha_Vencimiento) {
        Fecha_Vencimiento = fecha_Vencimiento;
    }
}
