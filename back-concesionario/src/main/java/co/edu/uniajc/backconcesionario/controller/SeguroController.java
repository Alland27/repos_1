package co.edu.uniajc.backconcesionario.controller;

import co.edu.uniajc.backconcesionario.modelo.Seguro;
import co.edu.uniajc.backconcesionario.service.SeguroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/seguro")
public class SeguroController {
    @Autowired
    public SeguroService seguroService;

    public SeguroController(SeguroService service){}

    public SeguroController(){}

    @PostMapping
    private ResponseEntity<Seguro> guardar(@RequestBody Seguro seguro){
        Seguro temp = seguroService.createSeguro(seguro);
        try{
            return ResponseEntity.created(new URI("/api/seguro/" + temp.getId_Seguro())).body(temp);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Seguro>> ListarTodosLosSeguros(){
        return ResponseEntity.ok(seguroService.ListarSeguros());
    }

    @GetMapping(value = "{id}")
    private ResponseEntity<Optional<Seguro>> ListarSeguroPorId(@PathVariable("id") Long id){
        return ResponseEntity.ok(seguroService.SeguroPorId(id));
    }

    @DeleteMapping
    private ResponseEntity<Void> EliminarSeguro(@RequestBody Seguro seguro){
        seguroService.DeleteSeguro(seguro);
        return ResponseEntity.ok().build();
    }

    public String analysemood(String message){return "sad";}
}
