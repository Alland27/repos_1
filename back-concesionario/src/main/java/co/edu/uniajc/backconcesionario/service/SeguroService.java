package co.edu.uniajc.backconcesionario.service;

import co.edu.uniajc.backconcesionario.modelo.Seguro;
import co.edu.uniajc.backconcesionario.repositorio.SeguroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SeguroService {
    @Autowired
    private SeguroRepositorio seguroRepositorio;

    public Seguro createSeguro(Seguro seguro){return seguroRepositorio.save(seguro);}

    public List<Seguro> ListarSeguros(){return seguroRepositorio.findAll();}

    public Optional<Seguro> SeguroPorId(Long Id){return seguroRepositorio.findById(Id);}

    public void DeleteSeguro (Seguro seguro){seguroRepositorio.delete(seguro);}
}