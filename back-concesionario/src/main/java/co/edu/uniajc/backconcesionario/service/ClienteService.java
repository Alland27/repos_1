package co.edu.uniajc.backconcesionario.service;

import co.edu.uniajc.backconcesionario.modelo.Cliente;
import co.edu.uniajc.backconcesionario.repositorio.ClienteRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

  @Autowired
  private ClienteRepositorio clienteRepositorio;

  public Cliente createCliente (Cliente cliente){
      return clienteRepositorio.save(cliente);

  }

  public List<Cliente> ListarClientes (){
      return clienteRepositorio.findAll();
  }


    public Optional<Cliente> ClienteporID (Long id){
        return clienteRepositorio.findById(id);
    }

    public void  DeleteCliente (Cliente cliente){
         clienteRepositorio.delete(cliente);

    }




}
