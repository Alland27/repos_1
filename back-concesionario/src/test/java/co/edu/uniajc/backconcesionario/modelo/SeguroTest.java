package co.edu.uniajc.backconcesionario.modelo;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SeguroTest {

    private static final Long ID_SEGURO = 1L;

    private static final Long ID_AUTO = 3L;

    private static final Long ID_PROPIETARIO = 1L;

    private static Date FECHA_EXPEDICION;
    {
        try {
            FECHA_EXPEDICION = new SimpleDateFormat("dd/MM/yyyy").parse("7/9/2021");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



    private static Date FECHA_VENCIMIENTO;
    {
        try {
            FECHA_VENCIMIENTO = new SimpleDateFormat("dd/MM/yyyy").parse("7/9/2022");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    Seguro pruebaS = new Seguro(ID_SEGURO, ID_AUTO, ID_PROPIETARIO, FECHA_EXPEDICION, FECHA_VENCIMIENTO);
    private static Long Nauto = 10L;
    private static Long Npro = 11L;
    private static Date FECHA_EXPEDICION2 ;
    {
        try {
            FECHA_EXPEDICION2 = new SimpleDateFormat("dd/MM/yyyy").parse("11/09/2022");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    private static Date FECHA_VENCIMIENTO2 ;
    {
        try {
            FECHA_VENCIMIENTO2 = new SimpleDateFormat("dd/MM/yyyy").parse("15/09/2022");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }




    @Test
    void getId_Seguro(){
        assertEquals(ID_SEGURO, pruebaS.getId_Seguro());
        assertNotNull(pruebaS.getId_Seguro());
    }

    @Test
    void setId_Seguro(){
        pruebaS.setId_Seguro(Nauto);
        assertEquals(  Nauto, pruebaS.getId_Seguro());
    }

    @Test
    void getId_Auto(){
        assertEquals(ID_AUTO, pruebaS.getId_Auto());
        assertNotNull(pruebaS.getId_Propietario());
    }

    @Test
    void etId_Auto(){
        pruebaS.setId_Auto(Nauto);
        assertEquals(  Nauto, pruebaS.getId_Auto());

    }



    @Test
    void getId_Propietario(){
        assertEquals(ID_PROPIETARIO, pruebaS.getId_Propietario());
        assertNotNull(pruebaS.getId_Propietario());
    }

    @Test
    void setId_Propietario(){
        pruebaS.setId_Propietario(Npro);
        assertEquals(  Npro, pruebaS.getId_Propietario());
    }

    @Test
    void getFecha_Expedicion(){
        assertEquals(FECHA_EXPEDICION, pruebaS.getFecha_Expedicion());
        assertNotNull(pruebaS.getFecha_Expedicion());
    }

    @Test
    void setFecha_Expedicion(){
        pruebaS.setFecha_Expedicion(FECHA_EXPEDICION2);
        assertEquals(  FECHA_EXPEDICION2, pruebaS.getFecha_Expedicion());
    }

    @Test
    void getFecha_Vencimiento(){
        assertEquals(FECHA_VENCIMIENTO, pruebaS.getFecha_Vencimiento());
        assertNotNull(pruebaS.getFecha_Vencimiento());
    }

    @Test
    void setFecha_Vencimiento(){
        pruebaS.setFecha_Vencimiento(FECHA_VENCIMIENTO2);
        assertEquals(  FECHA_VENCIMIENTO2, pruebaS.getFecha_Vencimiento());
    }
}
