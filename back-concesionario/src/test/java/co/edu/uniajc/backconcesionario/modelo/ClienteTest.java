package co.edu.uniajc.backconcesionario.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClienteTest {


    private static final Long NUMERO_D = 1111111111L;

    private static final String TIPO_D = "CEDULA";

    private static final String NOMBRE_C = "ALAN";

    private static final Long CELULAR = 3101221111L;

    private static final String DIRR = "CASA 123";

    private static final String CIUDAD  = "CALI";

    Cliente pruebaC =  new Cliente(NUMERO_D,TIPO_D,NOMBRE_C,CELULAR,DIRR,CIUDAD);
    private static Long Ncliente = 3333333L;
    private static Long Ncelular = 3113405678L;


    @Test
    void testGetNumero_Documento() {
        assertEquals(  NUMERO_D , pruebaC.getNumero_Documento());
        assertNotNull( pruebaC.getNumero_Documento()  );
    }

    @Test
    void setNumero_Documento() {
        pruebaC.setNumero_Documento(Ncliente);
        assertEquals(  Ncliente, pruebaC.getNumero_Documento());
    }

    @Test
    void testGetTipo_Documento() {
        assertEquals(  TIPO_D , pruebaC.getTipo_Documento());
        assertNotNull( pruebaC.getTipo_Documento()  );
    }

    @Test
    void setTipo_Documento() {
        pruebaC.setTipo_Documento("TARJETA");
        assertEquals(  "TARJETA" , pruebaC.getTipo_Documento());
    }

    @Test
    void testGetNombre_Cliente() {
        assertEquals(  NOMBRE_C , pruebaC.getNombre_Cliente());
        assertNotNull( pruebaC.getNombre_Cliente()  );
    }

    @Test
    void setNombre_Cliente() {
        pruebaC.setNombre_Cliente("GABRIEL");
        assertEquals(  "GABRIEL" , pruebaC.getNombre_Cliente());
    }

    @Test
    void getCelular() {
        assertEquals(  CELULAR , pruebaC.getCelular());
        assertNotNull( pruebaC.getCelular()  );
    }

    @Test
    void setCelular() {
        pruebaC.setCelular(Ncelular);
        assertEquals(  Ncelular , pruebaC.getCelular());
    }

    @Test
    void getDireccion_Cliente() {
        assertEquals(  DIRR , pruebaC.getDireccion_Cliente());
        assertNotNull( pruebaC.getDireccion_Cliente()  );
    }

    @Test
    void setDireccion_Cliente() {
        pruebaC.setDireccion_Cliente("CRA 27 E # 45-43");
        assertEquals(  "CRA 27 E # 45-43" , pruebaC.getDireccion_Cliente());
    }

    @Test
    void getCiudad() {
        assertEquals(  CIUDAD , pruebaC.getCiudad());
        assertNotNull( pruebaC.getCiudad()  );
    }

    @Test
    void setCiudad() {
        pruebaC.setCiudad("MEDELLIN");
        assertEquals(  "MEDELLIN" , pruebaC.getCiudad());
    }




}