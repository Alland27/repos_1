package co.edu.uniajc.backconcesionario.modelo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class VendedorTest {

    private static final Long NUMERO_D = 1111111L;

    private static final String TIPO_D = "CEDULA";

    private static final String NOMBRE_V = "PEPITO";

    private static final Long CELULAR = 31313131313L;

    private static final String DIRR = "CR 1 CLL 2-3";

    private static final String CIUDAD = "CALI";

    Vendedor pruebaV = new Vendedor(NUMERO_D, TIPO_D, NOMBRE_V, CELULAR, DIRR, CIUDAD);
    private static Long Nvendedor = 3333333L;
    private static Long Ncelular = 3113405678L;

    @Test
    void prueba_getNumero_Documento(){
        assertEquals(NUMERO_D, pruebaV.getNumero_Documento());
        assertNotNull(pruebaV.getNumero_Documento());
    }

    @Test
    void prueba_setNumero_Documento(){
        pruebaV.setNumero_Documento(Nvendedor);
        assertEquals(  Nvendedor, pruebaV.getNumero_Documento());
    }

    @Test
    void prueba_getTipo_Documento(){
        assertEquals(TIPO_D, pruebaV.getTipo_Documento());
        assertNotNull(pruebaV.getTipo_Documento());
    }

    @Test
    void prueba_setTipo_Documento(){
        pruebaV.setTipo_Documento("TARJETA");
        assertEquals(  "TARJETA" , pruebaV.getTipo_Documento());
    }

    @Test
    void prueba_getNombre_Vendedor(){
        assertEquals(NOMBRE_V, pruebaV.getNombre_Vendedor());
        assertNotNull(pruebaV.getNombre_Vendedor());
    }

    @Test
    void prueba_setNombre_Vendedor(){
        pruebaV.setNombre_Vendedor("JUANCHO");
        assertEquals(  "JUANCHO" , pruebaV.getNombre_Vendedor());
    }

    @Test
    void getCelular(){
        assertEquals(CELULAR, pruebaV.getCelular());
        assertNotNull(pruebaV.getCelular());
    }

    @Test
    void setCelular(){
        pruebaV.setCelular(Ncelular);
        assertEquals(  Ncelular , pruebaV.getCelular());
    }

    @Test
    void getDireccion(){
        assertEquals(DIRR, pruebaV.getDireccion_Vendedor());
        assertNotNull(pruebaV.getDireccion_Vendedor());
    }

    @Test
    void setDireccion(){
        pruebaV.setDireccion_Vendedor("CRA 27 E # 45-43");
        assertEquals(  "CRA 27 E # 45-43" , pruebaV.getDireccion_Vendedor());
    }

    @Test
    void getCiudad(){
        assertEquals(CIUDAD, pruebaV.getCiudad());
        assertNotNull(pruebaV.getCiudad());
    }

    @Test
    void prueba_setCiudad(){
        pruebaV.setCiudad("MEDELLIN");
        assertEquals(  "MEDELLIN" , pruebaV.getCiudad());
    }
}
